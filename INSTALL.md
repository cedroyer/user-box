# Install

## package

Currently, there is only a PKGBUILD available for archlinux, soon available in AUR.

## manual

if you want to install it in /usr/local/bin
run as root:
```
make BIN_DIR=/usr/local/bin install
```

if you want to install it in /opt/usr/bin
run as root:
```
make DESTDIR=/opt install
```

by default it we be install in /usr/bin (not recomended for manual installation)
