# User Box

## Install

see INSTALL.md

## Short description
User Box is a tools that let you easily create sub users.

## Get started
### Use for shell program
create a user box (as root):
```
ubboxadd myuser mybox
```

use a box (as myuser):
```
ubboxed mybox myprogram
```

### Use for graphical application
create a user box (as root) with x11 acces (-x) and/or sound-sharing (-s):
```
ubboxadd -s -x myuser mybox
```

use the a box (as myuser):
```
ubboxed -s -x mybox myprogram
```
>
**Note**: if you create a box with x11 or sound sharing (in **ubboxadd**), it can manage to have acces to it even if you don't add (-s -x) to **ubboxed** (see Securtiy for more inforamtion)
>

## More detail description
User Box is a set of bash script that help you to build this kind of structure:

| user                       | user box groups                          | primary group          |
| -------------------------- | ---------------------------------------- | ---------------------- |
|myuser                      | myuser-x11  myuser-mybox myuser-myx11box | *not a user box group* |
|myuser-mybox                | myuser-mybox                             | myuser-mybox           |
|myuser-myx11box             | myuser-myx11box myuser-x11               | myuser-myx11box        |

by default *myuser-mybox* home let a read write acces to group *myuser-mybox* that way *myuser* can acces files in his box.

when shared ressources (x11, sound...), **ubboxed** script manage to let this ressource accesible to the box used (by example let XAUTORITY file accessible to group myuser-x11)

## Dependancies

### Required
 - bash

### Optional
 - pulse-audio: for sound sharing
 - X11: for graphical application

## Security
### What User Box does not:
 - replace a chroot, a container system (Docker, systemd-nspawn...)
 - secure your computeur against many kind of attack
 - in particular, if you activate x11 sharing, the program can without effort spy your keyboard, your mouse and/or your screen... 
 - in particular, if you activate sound sharing, the program can without effort spy your microphone or any other sound input... 

### What User Box does:
 - avoid a particular program directly acces your file (if you correctly set the right, groups... of your home directory)
 - create a user with his config and context
 - in particular, open multiple time the same browser with differents set of cookies
 - in particular, open a voip program without give acces to all your personnal files


## TODO
 1. write 'ubboxdel' script
 2. secure in case of failure 
 3. think about a shared data group
 4. use unix socket to shared sound (actually shared using TCP)
 
