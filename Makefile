# user-box
# Copyright (C) 2017  Royer Cédric
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

DESTDIR:=''
BIN_DIR:="${DESTDIR}/usr/bin"

install: ${BIN_DIR}
	install -m 555 bin/ubboxadd ${BIN_DIR}
	install -m 555 bin/ubboxed  ${BIN_DIR}

${BIN_DIR}:
	mkdir -p "$@"

